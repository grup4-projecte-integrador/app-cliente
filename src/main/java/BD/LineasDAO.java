/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import static java.util.Arrays.asList;
import java.util.List;
import model.BatoilogicDireccion;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.BatoilogicLineaspedido;
import model.BatoilogicProducto;

/**
 *
 * @author batoi
 */
public class LineasDAO {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public LineasDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public ObservableList<BatoilogicLineaspedido> addAllFromPedido(int pedido_id) throws Exception {

        List<BatoilogicProducto> productos = new ProductosDAO().findAll();
        ObservableList<BatoilogicLineaspedido> lineas = FXCollections.observableArrayList();

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.lineaspedido", "search_read",
                asList(asList(
                        asList("pedido_id", "=", pedido_id)
                )),
                new HashMap() {
            {
                put("fields", asList("cantidad", "producto_id"));
            }
        }
        )));
        HashMap objetos = null;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            String nombre = "Undefined";
            int cantidad = (int) objetos.get("cantidad");
            int producto_id = (int) ((Object[]) objetos.get("producto_id"))[0];
            BatoilogicLineaspedido linea = new BatoilogicLineaspedido();
            
            for (BatoilogicProducto producto : productos) {
                if (producto.getId() == producto_id) {
                    linea.setBatoilogicProducto(producto);
                }
            }
            
            linea.setNombre(linea.getBatoilogicProducto().getNombre());
            linea.setCantidad(cantidad);
            
            lineas.add(linea);
        }
        return lineas;

    }

    public void insert(BatoilogicLineaspedido t) throws Exception {

        final Integer id = (Integer) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.lineaspedido", "create",
                asList(new HashMap() {
                    {
                        put("pedido_id", t.getBatoilogicPedido().getId());
                        put("producto_id", t.getBatoilogicProducto().getId());
                        put("cantidad", t.getCantidad());
                    }

                }
                )));

    }

}
