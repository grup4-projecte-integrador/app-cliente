/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import static java.util.Arrays.asList;
import java.util.List;
import model.BatoilogicCiudad;
import model.BatoilogicCliente;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

/**
 *
 * @author kurur
 */
public class CiudadDAO implements GenericoDAO<BatoilogicCiudad> {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public CiudadDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public boolean isCreated(String nombre) throws XmlRpcException {

        boolean exists = false;

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.Ciudad", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("nombre"));
            }
        }
        )));

        HashMap objetos;
        for (int i = 0; i < l.size() && !(exists); i++) {
            objetos = (HashMap) l.get(i);
            String aux = (String) objetos.get("nombre");
            if (aux.equalsIgnoreCase(nombre)) {
                exists = true;
            }
        }

        return exists;
    }

    public BatoilogicCiudad findByPK(int id) throws Exception {


        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.ciudad", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("nombre", "id"));
            }
        }
        )));

        BatoilogicCiudad ciudad = null;
        HashMap objetos;
        for (int i = 0; i < l.size() && ciudad == null; i++) {
            objetos = (HashMap) l.get(i);
            Integer aux = (Integer) objetos.get("id");
            if (aux.equals(id)) {
                ciudad = new BatoilogicCiudad(id, (String) objetos.get("nombre"));
            }
        }

        return ciudad;

    }

    @Override
    public List<BatoilogicCiudad> findAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicCiudad> findByExample(BatoilogicCiudad t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicCiudad> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(BatoilogicCiudad t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(BatoilogicCiudad t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BatoilogicCiudad findByPK(String user) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
