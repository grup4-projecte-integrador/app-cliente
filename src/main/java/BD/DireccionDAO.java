/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import static java.util.Arrays.asList;
import java.util.List;
import model.BatoilogicDireccion;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author batoi
 */
public class DireccionDAO implements GenericoDAO<BatoilogicDireccion> {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public DireccionDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public ObservableList<BatoilogicDireccion> findByUser(int user) throws Exception {

        ObservableList<BatoilogicDireccion> direcciones = FXCollections.observableArrayList();

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.direccion", "search_read",
                asList(asList(
                        asList("cliente_id", "=", user)
                )),
                new HashMap() {
            {
                put("fields", asList("id", "nombre", "tipo", "numero", "piso", "puerta"));
            }
        }
        )));
        HashMap objetos = null;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            direcciones.add(new BatoilogicDireccion((int) objetos.get("id"), (String) objetos.get("nombre"), (String) objetos.get("tipo"), (int) objetos.get("numero"), (int) objetos.get("piso"), (String) objetos.get("puerta")));
        }
        return direcciones;
    }

    public BatoilogicDireccion findByPk(int pk) throws Exception {

        ObservableList<BatoilogicDireccion> direcciones = FXCollections.observableArrayList();

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.direccion", "search_read",
                asList(asList(
                        asList("id", "=", pk)
                )),
                new HashMap() {
            {
                put("fields", asList("id", "nombre", "tipo", "numero", "piso", "puerta"));
            }
        }
        )));
        HashMap objetos = (HashMap) l.get(0);
        return new BatoilogicDireccion((int) objetos.get("id"), (String) objetos.get("nombre"), (String) objetos.get("tipo"), (int) objetos.get("numero"), (int) objetos.get("piso"), (String) objetos.get("puerta"));
    }

    @Override
    public List<BatoilogicDireccion> findAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicDireccion> findByExample(BatoilogicDireccion t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicDireccion> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(BatoilogicDireccion t) throws Exception {
        System.out.println(t.getBatoilogicPostal().getId() + " -> postal_id");
        final Integer id = (Integer) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.direccion", "create",
                asList(new HashMap() {
                    {
                        put("nombre", t.getNombre());
                        put("tipo", t.getTipo());
                        put("puerta", t.getPuerta());
                        put("numero", t.getNumero());
                        put("piso", t.getPiso());
                        put("cliente_id", t.getBatoilogicCliente().getId());
                        put("postal_id", t.getBatoilogicPostal().getId());
                    }

                }
                )));

    }

    @Override
    public boolean update(BatoilogicDireccion t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BatoilogicDireccion findByPK(String user) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
