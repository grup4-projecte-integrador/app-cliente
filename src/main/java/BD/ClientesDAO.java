/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import static java.util.Arrays.asList;
import java.util.Date;
import java.util.List;
import model.BatoilogicCliente;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

;

/**
 *
 * @author kurur
 */
public class ClientesDAO implements GenericoDAO<BatoilogicCliente> {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public ClientesDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    @Override
    public BatoilogicCliente findByPK(String user) throws Exception {

        ArrayList<BatoilogicCliente> clientes = new ArrayList<BatoilogicCliente>(1);
        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.cliente", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("id", "nombre", "apellidos", "passwd", "user", "fechanacimiento"));
            }
        }
        )));

        HashMap objetos;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            clientes.add(new BatoilogicCliente((int) objetos.get("id"), (String) objetos.get("nombre"), (String) objetos.get("apellidos"), (String) objetos.get("passwd"), (String) objetos.get("user"), (String) objetos.get("fechanacimiento")));
        }

        BatoilogicCliente clienteExistente = null;
        for (BatoilogicCliente cliente : clientes) {
            if (cliente.getUser().equals(user)) {
                clienteExistente = cliente;
            }
        }

        return clienteExistente;
    }

    @Override
    public List<BatoilogicCliente> findAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicCliente> findByExample(BatoilogicCliente t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicCliente> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(BatoilogicCliente t) throws Exception {

        final Integer id = (Integer) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.cliente", "create",
                asList(new HashMap() {
                    {
                        put("nombre", t.getNombre());
                        put("apellidos", t.getApellidos());
                        put("fechanacimiento", t.getFechanacimiento());
                        put("passwd", t.getPasswd());
                        put("user", t.getUser());
                    }

                }
                )));

    }

    @Override
    public boolean update(BatoilogicCliente t) throws Exception {

        bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.cliente", "write",
                asList(asList(t.getId()), new HashMap() {
                    {
                        put("nombre", t.getNombre());
                        put("apellidos", t.getApellidos());
                        put("fechanacimiento", t.getFechanacimiento());
                        put("passwd", t.getPasswd());
                        put("user", t.getUser());
                    }
                }
                )
        ));

        return false;
    }

    @Override
    public boolean delete(int id) throws Exception {

        return false;
    }

}
