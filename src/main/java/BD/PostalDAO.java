/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import java.util.List;
import model.BatoilogicPostal;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import java.util.HashMap;
import static java.util.Arrays.asList;
import model.BatoilogicCiudad;

/**
 *
 * @author kurur
 */
public class PostalDAO implements GenericoDAO<BatoilogicPostal> {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public PostalDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public BatoilogicPostal findByPkMod(String numero) throws XmlRpcException, MalformedURLException, Exception {

        boolean exists = false;

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.postal", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("id", "numero", "ciudad_id"));
            }
        }
        )));

        HashMap objetos;
        BatoilogicPostal batoilogicPostal = null;
        for (int i = 0; i < l.size() && !(exists); i++) {
            objetos = (HashMap) l.get(i);
            String auxNum = (String) objetos.get("numero");
            Object[] incognita = (Object[]) objetos.get("ciudad_id");
            Integer id = (Integer) objetos.get("id");
            for (int j = 0; j < incognita.length; j++) {
                System.out.println(incognita[j]);
            }
            int auxCiu = (int) incognita[0];
            BatoilogicCiudad ciudad = new CiudadDAO().findByPK(auxCiu);
            if (auxNum.equals(numero)) {
                batoilogicPostal = new BatoilogicPostal(id, ciudad, auxNum);

            }
        }

        return batoilogicPostal;
    }

    @Override
    public BatoilogicPostal findByPK(String user) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicPostal> findAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicPostal> findByExample(BatoilogicPostal t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicPostal> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(BatoilogicPostal t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(BatoilogicPostal t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
