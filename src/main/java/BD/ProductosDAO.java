/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import static java.util.Arrays.asList;
import java.util.Date;
import java.util.List;
import model.BatoilogicProducto;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

/**
 *
 * @author batoi
 */
public class ProductosDAO implements GenericoDAO<BatoilogicProducto> {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public ProductosDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public List<BatoilogicProducto> search(String busqueda) throws XmlRpcException {

        ArrayList<BatoilogicProducto> productos = new ArrayList<BatoilogicProducto>();
        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.producto", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("id", "nombre", "descripcion", "stock", "precio"));
            }
        }
        )));

        HashMap objetos;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            String descripcion = "";
            if(objetos.get("descripcion") instanceof Boolean){
                descripcion = "Sin descripcion";
            } else {
                descripcion = (String) objetos.get("descripcion");
            }
            if (((String) objetos.get("nombre")).contains(busqueda) || (descripcion).contains(busqueda)) {
                productos.add(new BatoilogicProducto((int) objetos.get("id"), (String) objetos.get("nombre"), descripcion, (int) objetos.get("stock"), (Double) objetos.get("precio")));
            }
        }

        return productos;
    }

    @Override
    public BatoilogicProducto findByPK(String user) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Double getPrecio(int id) throws Exception {

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.producto", "search_read",
                asList(asList(
                        asList("id", "=", id)
                )),
                new HashMap() {
            {
                put("fields", asList("precio"));
            }
        }
        )));

        return (Double) ((HashMap) l.get(0)).get("precio");

    }

    @Override
    public List<BatoilogicProducto> findAll() throws Exception {

        ArrayList<BatoilogicProducto> productos = new ArrayList<BatoilogicProducto>();
        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.producto", "search_read",
                asList(asList()),
                new HashMap() {
            {
                put("fields", asList("id", "nombre", "descripcion", "stock", "precio"));
            }
        }
        )));

        HashMap objetos;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            String descripcion = "";
            if(objetos.get("descripcion") instanceof Boolean){
                descripcion = "Sin descripcion";
            } else {
                descripcion = (String) objetos.get("descripcion");
            }
            productos.add(new BatoilogicProducto((int) objetos.get("id"), (String) objetos.get("nombre"), descripcion, (int) objetos.get("stock"), (Double) objetos.get("precio")));
        }

        return productos;

    }

    @Override
    public List<BatoilogicProducto> findByExample(BatoilogicProducto t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoilogicProducto> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(BatoilogicProducto t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(BatoilogicProducto t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
