/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import static java.util.Arrays.asList;
import java.util.Date;
import java.util.List;
import model.BatoilogicDireccion;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.BatoilogicPedido;

/**
 *
 * @author batoi
 */
public class PedidoDAO {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient bd;

    public PedidoDAO() throws MalformedURLException, XmlRpcException {
        bd = base.getInstance();
    }

    public ObservableList<BatoilogicPedido> findByUser(int user) throws Exception {

        ObservableList<BatoilogicPedido> pedidos = FXCollections.observableArrayList();

        List l = asList((Object[]) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.pedido", "search_read",
                asList(asList(
                        asList("cliente_id", "=", user)
                )),
                new HashMap() {
            {
                put("fields", asList("id", "estado", "fechaestimada", "fechapedido", "direccion_id", "preciototal"));
            }
        }
        )));
        HashMap objetos = null;
        for (int i = 0; i < l.size(); i++) {
            objetos = (HashMap) l.get(i);
            Object[] objects = (Object[]) objetos.get("direccion_id");
            int direccion_id = (int) objects[0];
            BatoilogicDireccion direccion = (new DireccionDAO().findByPk(direccion_id));

            pedidos.add(new BatoilogicPedido((int) objetos.get("id"), direccion, (Double) objetos.get("preciototal"), (String) objetos.get("observacion"), (String) objetos.get("fechapedido"),
                    (String) objetos.get("fechaestimada"), (String) objetos.get("estado")));
        }
        return pedidos;
    }

    public Integer insert(BatoilogicPedido t) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fechaPedido = sdf.format(t.getFechapedido());
        String fechaEntrega = sdf.format(t.getFechaestimada());

        final Integer id = (Integer) bd.execute("execute_kw", asList(
                base.DB, base.uid, base.PASS,
                "batoilogic.pedido", "create",
                asList(new HashMap() {
                    {
                        put("cliente_id", t.getBatoilogicCliente().getId());
                        put("direccion_id", t.getBatoilogicDireccion().getId());
                        put("observacion", t.getObservacion());
                        put("fechapedido", fechaPedido);
                        put("fechaestimada", fechaEntrega);
                        put("preciototal", t.getPrecioTotal());
                        put("estado", t.getEstado());
                    }

                }
                )));

        return id;

    }

}
