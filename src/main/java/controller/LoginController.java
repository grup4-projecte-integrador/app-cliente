/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.ClientesDAO;
import dam.grup4.cipfpbatoi.appclient.App;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.BatoilogicCliente;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class LoginController implements Initializable {

    @FXML
    private TextField tfUserLog;
    @FXML
    private PasswordField tfPassLog;
    @FXML
    private TextField tfNomReg;
    @FXML
    private TextField tfApellidoReg;
    @FXML
    private TextField tfUserReg;
    @FXML
    private PasswordField tfPassReg;
    @FXML
    private PasswordField tfPassRepReg;
    @FXML
    private DatePicker dpBirthDateReg;
    @FXML
    private AnchorPane pRegister;
    @FXML
    private Button btCerrar;

    protected ClientesDAO opCli;
    protected static BatoilogicCliente cliente;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        try {
            opCli = new ClientesDAO();
            cliente = new BatoilogicCliente();
        } catch (MalformedURLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XmlRpcException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void login() {

        try {
            String user = tfUserLog.getText();
            if (user.length() <= 1) {
                throw new Exception("El campo de usuario es erroneo.");
            }
            String pass = tfPassLog.getText();
            if ((cliente = opCli.findByPK(user)) != null) {

                if (cliente.getPasswd().equals(pass)) {
                    System.out.println("ID DEL CLIENTE: " + cliente.getId());
                    enterToApp();
                } else {
                    throw new Exception("Credenciales incorrectas");
                }

            } else {
                throw new Exception("Credenciales incorrectas.");
            }
        } catch (Exception ex) {
            showErrorAlert(ex.getMessage());
        }

    }

    public void cpRegister() {

        pRegister.setVisible(true);

    }

    public void cpLogin() {
        emptyRegister();
        pRegister.setVisible(false);
    }

    public void register() {

        try {
            String nombre = tfNomReg.getText();
            if (nombre.length() <= 2 || nombre.length() >= 40) {
                throw new Exception("El nombre debe tener entre 3 y 39 caracteres");
            }
            String apellidos = tfApellidoReg.getText();
            if (apellidos.length() <= 2 || apellidos.length() >= 40) {
                throw new Exception("Los apellidos deben tener entre 3 y 39 caracteres");
            }
            String user = tfUserReg.getText();
            if (user.length() <= 2 || user.length() >= 20) {
                throw new Exception("El usuario debe tener entre 3 y 19 caracteres");
            }
            String pass = tfPassReg.getText();
            if (pass.length() <= 2 || pass.length() >= 20) {
                throw new Exception("La contrasenya debe tener entre 3 y 19 caracteres");
            }
            String passRep = tfPassRepReg.getText();
            if (!passRep.equals(pass)) {
                throw new Exception("Las contrasenyas no coinciden");
            }
            LocalDate localDate = dpBirthDateReg.getValue();
            System.out.println(localDate.getYear());
            if (localDate.getYear() > 2005) {
                throw new Exception("Debes tener al menos 16 anyos para registrarse");
            }

            BatoilogicCliente checkUser = opCli.findByPK(user);
            if (checkUser != null) {
                throw new Exception("Ya hay un \"" + user + "\" registrado");
            }

            String fecNac = localDate.getYear() + "-" + localDate.getMonthValue() + "-" + localDate.getDayOfMonth();
            opCli.insert(new BatoilogicCliente(nombre, apellidos, pass, user, fecNac));
            showInformationAlert("Se ha creado el usuario correctamente.");
            pRegister.setVisible(false);
            emptyRegister();
        } catch (Exception ex) {
            showErrorAlert(ex.getMessage());
        }

    }

    protected void showErrorAlert(String msg) {
        Alert a = new Alert(Alert.AlertType.ERROR, msg, ButtonType.CLOSE);
        a.setTitle("ERROR");
        a.show();
    }

    protected void showInformationAlert(String msg) {
        Alert a = new Alert(Alert.AlertType.INFORMATION, msg, ButtonType.CLOSE);
        a.setTitle("INFO");
        a.show();
    }

    public void enterToApp() {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("main.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.show();
            ((Stage) tfNomReg.getScene().getWindow()).close();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void emptyRegister() {
        tfNomReg.setText("");
        tfApellidoReg.setText("");
        tfPassReg.setText("");
        tfPassRepReg.setText("");
        tfUserReg.setText("");
    }

    public void cerrar() {
        ((Stage) btCerrar.getScene().getWindow()).close();
        System.exit(0);
    }

}
