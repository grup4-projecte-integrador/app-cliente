/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.CiudadDAO;
import BD.ClientesDAO;
import BD.ConexionBD;
import BD.DireccionDAO;
import BD.LineasDAO;
import BD.PedidoDAO;
import BD.PostalDAO;
import BD.ProductosDAO;
import dam.grup4.cipfpbatoi.appclient.App;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import static java.util.Arrays.asList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.web.*;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.BatoilogicAlbaran;
import model.BatoilogicCliente;
import model.BatoilogicDireccion;
import model.BatoilogicLineaspedido;
import model.BatoilogicPedido;
import model.BatoilogicPostal;
import model.BatoilogicProducto;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

/**
 *
 * @author batoi
 */
public class MainController extends LoginController implements Initializable {

    @FXML
    private AnchorPane apPedido;
    @FXML
    private AnchorPane apHistorial;
    @FXML
    private AnchorPane apInicio;
    @FXML
    private AnchorPane apCarrito;
    @FXML
    private AnchorPane apConfirmarPedido;
    @FXML
    private AnchorPane apPerfil;
    @FXML
    private TableView tvProductos;
    @FXML
    private TableView tvPedidos;
    @FXML
    private TableView tvProdHist;
    @FXML
    private TableView tvCarrito;
    @FXML
    private TextField tfContrasenya;
    @FXML
    private TextField tfContrasenyaRep;
    @FXML
    private TextField tfNombreDir;
    @FXML
    private ComboBox cbTipoDir;
    @FXML
    private ComboBox cbDirEnv;
    @FXML
    private TextField tfNumeroDir;
    @FXML
    private TextField tfPisoDir;
    @FXML
    private TextField tfPuertaDir;
    @FXML
    private TextField tfPostalDir;
    @FXML
    private TextField tfBuscarProductos;
    @FXML
    private TextArea tfObservacionesEnv;
    @FXML
    private Label lCantProd;
    @FXML
    private Label lEstado;
    @FXML
    private Label lFecEst;
    @FXML
    private Label lFecCrea;
    @FXML
    private Label lPrecioTotal;
    @FXML
    private WebView wvMap;

    private ProductosDAO opProd;
    private DireccionDAO opDir;
    private CiudadDAO opCiu;
    private PostalDAO opPostal;
    private PedidoDAO opPedido;
    private LineasDAO opLineas;
    private ObservableList<BatoilogicProducto> productList;
    private ObservableList<BatoilogicLineaspedido> carritoList;
    private ObservableList<BatoilogicDireccion> direccionList;
    private ObservableList<BatoilogicPedido> pedidosList;
    private ObservableList<BatoilogicLineaspedido> lineasList;

    private BatoilogicPedido pedidoActual;
    private Double precioTotal;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {

            opCli = new ClientesDAO();
            opDir = new DireccionDAO();
            opCiu = new CiudadDAO();
            opPostal = new PostalDAO();
            opPedido = new PedidoDAO();
            opLineas = new LineasDAO();
            precioTotal = 0.0;
        } catch (MalformedURLException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XmlRpcException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        cpInicio();
        initProductos();
        initTvProductos();
        initTvCarrito();
        initCbTipoDireccion();
        initCbDireccion();
        initTvPedidos();
        initTvLineas();
        tfObservacionesEnv.setWrapText(true);
        newPedido();

    }

    public void cpPedido() {

        showInformationAlert("Para agregar un producto al carrito, haga doble click en él");
        apPedido.toFront();

    }

    public void cpHistorial() {

        try {
            initTvPedidos();
            apHistorial.toFront();
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cpInicio() {

        apInicio.toFront();

    }

    public void cpCarrito() {

        apCarrito.toFront();

    }

    public void cpConfirmarPedido() {

        try {
            if (carritoList.size() == 0) {
                throw new Exception("Tienes el carrito vacio.");
            }
            apConfirmarPedido.toFront();
        } catch (Exception ex) {
            showErrorAlert(ex.getMessage());
        }

    }

    public void buscarProductos() {
        String buscar = tfBuscarProductos.getText();

        if (buscar.isEmpty()) {
            try {
                productList.clear();
                productList.addAll(opProd.findAll());
            } catch (Exception ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {

            try {
                productList.clear();
                productList.addAll(opProd.search(buscar));
            } catch (XmlRpcException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        tvProductos.refresh();

    }

    public void agregarProducto(MouseEvent e) {
        try {
            if (e.getClickCount() == 2) {
                TextInputDialog tid = new TextInputDialog();
                tid.setTitle("Introduzca la cantidad");
                Optional<String> boton = tid.showAndWait();
                int cantidad = Integer.parseInt(tid.getEditor().getText().toString());

                if (!boton.isPresent()) {
                    throw new Exception("Cancelado con exito.");
                }

                if (cantidad <= 0) {
                    showErrorAlert("Cantidad erronea");
                } else {
                    BatoilogicProducto producto = (BatoilogicProducto) tvProductos.getSelectionModel().getSelectedItem();
                    BatoilogicLineaspedido linea = new BatoilogicLineaspedido();
                    linea.setCantidad(cantidad);
                    linea.setBatoilogicPedido(pedidoActual);
                    linea.setBatoilogicProducto(producto);
                    linea.setNombre(producto.getNombre());

                    carritoList.add(linea);
                    updatePrecioTotal();
                    actualizarLabelCarrito();
                }

            }
        } catch (NumberFormatException ex) {
            showErrorAlert("Debes introducir un numero.");
        } catch (Exception ex) {

        }

    }

    public void initTvLineas() {
        lineasList = FXCollections.observableArrayList();

        TableColumn<BatoilogicLineaspedido, String> colNombre = new TableColumn<>("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        TableColumn<BatoilogicLineaspedido, Integer> colCant = new TableColumn<>("Cantidad");
        colCant.setCellValueFactory(new PropertyValueFactory<>("cantidad"));

        tvProdHist.getColumns().addAll(colNombre, colCant);
        tvProdHist.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvProdHist.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvProdHist.setItems(lineasList);

    }

    public void updateLineas() {

        try {
            lineasList.clear();
            lineasList.addAll(opLineas.addAllFromPedido(((BatoilogicPedido) tvPedidos.getSelectionModel().getSelectedItem()).getId()));
            tvProdHist.refresh();
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void initTvProductos() {

        TableColumn<BatoilogicProducto, String> colNombre = new TableColumn<>("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        TableColumn<BatoilogicProducto, Date> colDesc = new TableColumn<>("Descripcion");
        colDesc.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        TableColumn<BatoilogicProducto, Double> colPrecio = new TableColumn<>("Precio");
        colPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));
        tvProductos.getColumns().clear();
        tvProductos.getColumns().addAll(colNombre, colDesc, colPrecio);
        tvProductos.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvProductos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvProductos.setItems(productList);

    }

    public void initTvCarrito() {

        carritoList = FXCollections.observableArrayList();

        TableColumn<BatoilogicLineaspedido, String> colNombre = new TableColumn<>("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        TableColumn<BatoilogicLineaspedido, Integer> colCant = new TableColumn<>("Cantidad");
        colCant.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
        TableColumn<BatoilogicLineaspedido, String> colPrecio = new TableColumn<>("Precio");
        colPrecio.setCellValueFactory(cellData -> cellData.getValue().getPreciototalString());

        tvCarrito.getColumns().addAll(colNombre, colCant, colPrecio);
        tvCarrito.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvCarrito.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvCarrito.setItems(carritoList);

    }

    public void initTvPedidos() {

        try {
            pedidosList = opPedido.findByUser(cliente.getId());
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

        TableColumn<BatoilogicPedido, BatoilogicDireccion> colDir = new TableColumn<>("Direccion");
        colDir.setCellValueFactory(new PropertyValueFactory<>("BatoilogicDireccion"));
        TableColumn<BatoilogicPedido, Double> colPrecio = new TableColumn<>("Precio");
        colPrecio.setCellValueFactory(new PropertyValueFactory<>("precioTotal"));

        tvPedidos.getColumns().clear();
        tvPedidos.getColumns().addAll(colDir, colPrecio);
        tvPedidos.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvPedidos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvPedidos.setItems(pedidosList);

    }

    public void enviarPedido() {
        try {
            if (cbDirEnv.getSelectionModel().getSelectedItem() == null) {
                throw new Exception("Debes seleccionar una direccion");
            }
            pedidoActual.setObservacion(tfObservacionesEnv.getText().toString());
            pedidoActual.setFechapedido(new Date());
            pedidoActual.setFechaestimada(addDays(new Date(), 2));
            pedidoActual.setBatoilogicDireccion((BatoilogicDireccion) cbDirEnv.getSelectionModel().getSelectedItem());
            pedidoActual.setBatoilogicCliente(cliente);
            pedidoActual.setPrecioTotal(precioTotal);
            pedidoActual.setEstado("EP");
            pedidoActual.setId(opPedido.insert(pedidoActual));

            for (BatoilogicLineaspedido linea : carritoList) {
                opLineas.insert(linea);
            }

            initTvPedidos();
            newPedido();
            cpInicio();
            showInformationAlert("Pedido enviado. Usted puede ver su estado en el historial.");
        } catch (Exception ex) {
            showErrorAlert(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void mostrarPedido(MouseEvent e) {

        BatoilogicPedido aux = (BatoilogicPedido) tvPedidos.getSelectionModel().getSelectedItem();

        String axu = aux.getEstado();
        switch (axu) {
            case "EP":
                lEstado.setText("En preparacion");
                break;
            case "Preparado":
                lEstado.setText("Preparado");
                break;
            case "ER":
                lEstado.setText("En ruta");
                break;
            case "E":
                lEstado.setText("Entregado");
                break;
            case "NE":
                lEstado.setText("No entregado");
                break;
        }

        lFecCrea.setText(aux.getFechapedidoS());
        lFecEst.setText(aux.getFechaestimadaS());
        updateLineas();

    }

    public void newPedido() {
        pedidoActual = new BatoilogicPedido();
        carritoList.clear();
        tvCarrito.refresh();
        lPrecioTotal.setText("0.0€");
        lCantProd.setText("0");
        tfObservacionesEnv.setText("");
    }

    public void actualizarLabelCarrito() {
        lCantProd.setText("" + carritoList.size());
    }

    public void quitarProducto() {

        BatoilogicLineaspedido linea = (BatoilogicLineaspedido) tvCarrito.getSelectionModel().getSelectedItem();
        carritoList.remove(linea);
        tvCarrito.refresh();
        actualizarLabelCarrito();
        updatePrecioTotal();

    }

    public void cpPerfil() {
        apPerfil.toFront();
    }

    public void initProductos() {

        try {
            opProd = new ProductosDAO();
            productList = FXCollections.observableArrayList();
            productList.addAll(opProd.findAll());
        } catch (MalformedURLException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XmlRpcException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void initCbTipoDireccion() {

        ObservableList<String> tipos = FXCollections.observableArrayList();
        tipos.add("calle");
        tipos.add("avenida");
        tipos.add("plaza");
        tipos.add("camino");
        tipos.add("pasaje");
        cbTipoDir.setItems(tipos);
        cbTipoDir.getSelectionModel().select(0);

    }

    public void initCbDireccion() {

        try {
            direccionList = opDir.findByUser(cliente.getId());
            cbDirEnv.setItems(direccionList);
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updatePrecioTotal() {
        precioTotal = 0.0;
        for (BatoilogicLineaspedido linea : carritoList) {
            precioTotal += linea.getPrecioTotal();
        }
        lPrecioTotal.setText(precioTotal + "€");
    }

    public void cambiarContrasenya() {

        if (tfContrasenya.getText().length() < 3) {
            showErrorAlert("La password debe tener un minimo de 3 caracteres");
        } else if (tfContrasenya.getText().equals(tfContrasenyaRep.getText())) {
            cliente.setPasswd(tfContrasenya.getText());
            try {
                opCli.update(cliente);
                showInformationAlert("Contrasenya cambiada correctamente");
            } catch (Exception ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            showErrorAlert("Las password deben coincidir.");
        }

    }

    public void anyadirDireccion() {
        try {

            String tipo = cbTipoDir.getSelectionModel().getSelectedItem().toString();
            String nombre = "";
            if (tfNombreDir.getText().length() < 3) {
                throw new Exception("La direccion es demasiado corta.");
            } else {
                nombre = tfNombreDir.getText();
            }
            if (tfNumeroDir.getText().toString().length() == 0 || tfNumeroDir.getText().toString().length() > 4) {
                throw new Exception("El numero es demasiado largo o no esta especificado.");
            }
            int numero = Integer.parseInt(tfNumeroDir.getText().toString());
            int piso;
            if (tfPisoDir.getText().toString().length() == 0) {
                piso = 0;
            } else {
                piso = Integer.parseInt(tfPisoDir.getText().toString());
            }
            String puerta = "";
            if (tfPuertaDir.getText().toString().length() == 0) {
                puerta = "";
            } else if (tfPuertaDir.getText().toString().length() > 2) {
                throw new Exception("Puerta no valida.");
            } else {
                puerta = tfPuertaDir.getText().toString();
            }
            String postal = tfPostalDir.getText().toString();
            if (postal.length() < 1 || postal.length() > 5) {
                throw new Exception("El codigo postal es incorrecto");
            }
            BatoilogicPostal codigoPostal;
            if ((codigoPostal = opPostal.findByPkMod(postal)) == null) {
                throw new Exception("Ese codigo postal no consta en nuestra base de datos.");
            }
            opDir.insert(new BatoilogicDireccion(cliente, codigoPostal, nombre, tipo, numero, piso, puerta));
            showInformationAlert("Se ha creado la nueva direccion con exito.");

            initCbDireccion();

        } catch (NumberFormatException ex) {
            showErrorAlert("Algun caracter invalido donde deberias introducir un numero...?");
        } catch (Exception ex) {
            showErrorAlert(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void cerrarSesion() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("login.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.show();
            ((Stage) apHistorial.getScene().getWindow()).close();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Date addDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }
    

}
