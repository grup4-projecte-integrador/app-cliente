/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author kurur
 */
public class Pedido 
{
    private int id;
    private String descripcion;
    private String estado;
    private String observaciones;
    private float precioTotal;
    private Date fechaEstimada;
    private Date fechaPedido;

    public Pedido(int id, String descripcion, String estado, String observaciones, float precioTotal, Date fechaEstimada, Date fechaPedido) {
        this.id = id;
        this.descripcion = descripcion;
        this.estado = estado;
        this.observaciones = observaciones;
        this.precioTotal = precioTotal;
        this.fechaEstimada = fechaEstimada;
        this.fechaPedido = fechaPedido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    public Date getFechaEstimada() {
        return fechaEstimada;
    }

    public void setFechaEstimada(Date fechaEstimada) {
        this.fechaEstimada = fechaEstimada;
    }

    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Override
    public String toString() {
        return "Pedido{" + "id=" + id + ", descripcion=" + descripcion + ", estado=" + estado + ", observaciones=" + observaciones + ", precioTotal=" + precioTotal + ", fechaEstimada=" + fechaEstimada + ", fechaPedido=" + fechaPedido + '}';
    }
}
